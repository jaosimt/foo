'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import './PrettyToastr.css'

export default class PrettyToastr extends React.Component {
    render() {
        const {title, message, list, collapsedItems} = this.props;
        
        return (<div className="pretty-toastr-message-renderer">
            {!message ? '' :
                <div>
                  <h4>{String(title).toLowerCase() === message.toLowerCase() ? '' : message}</h4>
                    {list ? <ul>
                        {list.map((l) => <li>{l}</li>)}
                    </ul> : ''}
                </div>
            }
        </div>);
    }
}

PrettyToastr.propTypes = {
    title: PropTypes.string,
    message: PropTypes.string.isRequired,
    list: PropTypes.array,
    collapsedItems: PropTypes.array
};
