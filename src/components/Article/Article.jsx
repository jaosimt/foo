'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import './Article.scss'
import HideIf from "../HideIf"
import { isFunction, isString } from '../../utils/sImoUtils'

export default class Article extends React.Component {
    constructor(props) {
        super(props);
        
        this.articleClick.bind(this);
        this.closeClick.bind(this);
    
        this.componentClickHandler.bind(this);
        this.componentKeyPressHandler.bind(this);
    }
    
    componentKeyPressHandler = ({keyCode}) => {
        if (keyCode === 27 && isFunction(this.props.onClose)) {
            this.props.onClose();
        }
    };
    
    componentClickHandler = ({currentTarget, target}) => {
        if (target.classList.contains('foo-collapsible-content') && isFunction(this.props.onClose)) {
            this.props.onClose()
        }
    };
    
    closeClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        
        if (isFunction(this.props.onClose)) {
            this.props.onClose();
        }
    };
    
    articleClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        
        if (isFunction(this.props.onClick)) {
            this.props.onClick(e, this.props.size);
        }
    };
    
    componentDidMount() {
        if (isFunction(this.props.onClose)) {
            document.addEventListener('mousedown', this.componentClickHandler, false);
            document.addEventListener('keyup', this.componentKeyPressHandler, false);
        }
        
        if (isFunction(this.props.onMount)) {
            this.props.onMount();
        }
    }
    
    componentWillUnmount() {
        if (isFunction(this.props.onClose)) {
            document.removeEventListener('mousedown', this.componentClickHandler, false);
            document.removeEventListener('keyup', this.componentKeyPressHandler, false);
        }
    }
    
    render() {
        const { article, width, size, ranked, className, onClose } = this.props;
        
        return (
            <div onClick={this.articleClick} data-uid={article.uid} className={'foo-article linear-transition' + (size === 'full' ? ' full' : '') + (isString(className) ? (' ' + className) : '')} style={{width: width}}>
                <div className={'image' + (size === 'small' ? ' small' : '')} style={{
                    backgroundImage: 'url(' + article.image + ')'
                }}>
                    <HideIf condition={!ranked}>
                        <div className={'ranking'}>{article.ranking}</div>
                    </HideIf>
                    <HideIf condition={!isFunction(onClose)}>
                        <div onClick={this.closeClick} className={'close'}/>
                    </HideIf>
                </div>
                <div className={'info' + (size === 'small' ? ' small' : '')}>
                    <div className={'category-date'}>
                        <div className={'category'}>{article.category}</div>
                        <div className={'date'}>{article.date}</div>
                    </div>
                    <div className={'title'}>{article.title}</div>
                </div>
                <HideIf condition={size !== 'small'}>
                    <div className={'clear-fix'}/>
                </HideIf>
            </div>
        );
    }
}

Article.propTypes = {
    article: PropTypes.object.isRequired,
    width: PropTypes.string,
    size: PropTypes.string,
    className: PropTypes.string,
    ranked: PropTypes.bool,
    onClick: PropTypes.func,
    onMount: PropTypes.func,
    onClose: PropTypes.func
};

Article.defaultProps = {
    width: '100%',
    size: 'default',
    ranked: false,
    onClose: null,
    onMount: null,
    onClick: null
};