'use strict';

import React from 'react'
import PropTypes from 'prop-types'
import './ImageSlider.scss'
import HideIf from "../HideIf";

export default class ImageSlider extends React.Component {
    //  percentage multiplier computation
    //  based on
    //              * containerWidth    1280px
    //              * center image      852px
    
    containerWidth = 1280;
    centerImage = 852;
    imageBorder = 2;
    
    percentMultiplier = this.centerImage / this.containerWidth;
    imageWidth = Math.round(this.containerWidth * this.percentMultiplier);
    centerOffset = Math.round((this.containerWidth - this.centerImage + this.imageBorder) / 2);
    sideWidth = this.containerWidth - this.imageWidth;
    
    state = {
        bannerLefts: []
    };
    
    hasLeft = false;
    hasRight = false;
    
    fooSliderRef = React.createRef();
    
    componentDidMount() {
        this.containerWidth = this.fooSliderRef.current.getBoundingClientRect().width;
        this.imageWidth = Math.round(this.containerWidth * this.percentMultiplier);
        this.centerOffset = Math.round((this.containerWidth - this.imageWidth + this.imageBorder) / 2);
        this.sideWidth = this.containerWidth - this.imageWidth;
    
        let left = this.centerOffset - this.imageWidth;
    
        const { data } = this.props,
            bLs = [];
        
        if (data.length > 1) {
            this.hasLeft = true;
        }
        
        if (data.length > 2) {
            this.hasRight = true
        }
        
        data.map(d => {
            bLs.push({left: left});
            left += this.imageWidth;
        });
        
        this.setState({bannerLefts: bLs, opacity: 1})
    }
    
    leftClickHandler = () => {
        const { bannerLefts } = this.state,
            bLs = [];
        
        this.hasRight = true;
        
        bannerLefts.map((l, i) => {
            const newLeft = l.left + this.imageWidth;
            
            if (i === 0 && newLeft === this.centerOffset) {
                this.hasLeft = false;
            }
            
            bLs.push({left: newLeft})
        });
        
        this.setState({bannerLefts: bLs})
    };
    
    rightClickHandler = () => {
        const { bannerLefts } = this.state,
            bLs = [];
        
        this.hasLeft = true;
        
        bannerLefts.map((l, i) => {
            const newLeft = l.left - this.imageWidth;
            if (i === (bannerLefts.length - 1) && newLeft === this.centerOffset) {
                this.hasRight = false;
            }
            
            bLs.push({left: newLeft})
        });
        
        this.setState({bannerLefts: bLs})
    };
    
    render() {
        const { bannerLefts } = this.state,
            { data } = this.props;
        
        return (<div
            className={'foo-image-slider'}
            ref={this.fooSliderRef}
        >
            {
                data.map((d, i) => {
                    const thisLeft = bannerLefts[i] ? bannerLefts[i].left : 0,
                        image = <div className={'img linear-transition'} style={
                            {
                                width: (this.imageWidth - this.imageBorder) + 'px',
                                backgroundImage: 'url(' + d.image + ')',
                                left: thisLeft + 'px'
                            }
                        }>
                            <div className={'caption'}>
                                <span className={'category'}>{d.category}</span>
                                <span className={'date'}>{d.date}</span>
                                <div className={'title'}>{d.title}</div>
                            </div>
                        </div>;
                    
                    return (image)
                })
            }
            <HideIf condition={this.hasLeft === false}>
                <div
                    className={'btn left linear-transition'}
                    onClick={this.leftClickHandler}
                    style={ {left: (this.sideWidth / 2) - 60} }
                />
            </HideIf>
            <HideIf condition={this.hasRight === false}>
                <div
                    className={'btn right linear-transition'}
                    onClick={this.rightClickHandler}
                    style={ {right: (this.sideWidth / 2) - 60} }
                />
            </HideIf>
        </div>);
    }
}

ImageSlider.propTypes = {
    data: PropTypes.object.isRequired
};