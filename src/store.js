'use strict';

import thunk from 'redux-thunk'
import { applyMiddleware, compose, combineReducers, createStore } from 'redux'
import { reducer as toastrReducer } from 'react-redux-toastr'
import bannerReducer from './reducers/bannerReducer'
import featuredReducer from './reducers/featuredReducer'
import articlesReducer from './reducers/articlesReducer'

import life1 from './images/articles/life/377195-1.jpeg'
import life2 from './images/articles/life/border-collie-667502_1280.jpg'
import life3 from './images/articles/life/Chicago_joya_life_2.jpg'
import life4 from './images/articles/life/globtrotter-entrepreneur-desk.jpg'
import life5 from './images/articles/life/Community_1544217191_1280x.jpg'
import life6 from './images/articles/life/Happiness-Image.jpg'
import life7 from './images/articles/life/tumblr_lvlyyqXD9O1r6cwtyo2_1280.jpg'
import life8 from './images/articles/life/Whats-for-lunch-1.jpg'
import life9 from './images/articles/life/tumblr_mb6h4lZVLy1r99ttio1_1280.jpg'
import life10 from './images/articles/life/giza-pyramids.jpg'
import life11 from './images/articles/life/meditation_1536041905.jpg'
import life12 from './images/articles/life/unspecified-1-1280x730.jpg'
import life13 from './images/articles/life/the_immortalists-1280x730.jpg'
import life14 from './images/articles/life/whale-shark-281498_1280.jpg'
import cultural1 from './images/articles/cultural/4d73c8d12604263a9c3e2f88a0848eac.jpg'
import cultural2 from './images/articles/cultural/103286429.jpg'
import cultural3 from './images/articles/cultural/africa-child-sponsorship-kamama.jpg'
import cultural4 from './images/articles/cultural/b97_5686a.jpg'
import cultural5 from './images/articles/cultural/BigDataCPS-2016-blog.jpg'
import cultural6 from './images/articles/cultural/d9f2ccdd1c8949b3b8d00e73d03a3e3f_th.png'
import cultural7 from './images/articles/cultural/Dd000046.jpg'
import cultural8 from './images/articles/cultural/life_expectancy.jpg'
import cultural9 from './images/articles/cultural/manipuri.jpg'
import cultural10 from './images/articles/cultural/MasquesMoulinRougeSBertrand.jpg'
import cultural11 from './images/articles/cultural/r8i2xl8wmvnpyf7ommzt.jpg'
import cultural12 from './images/articles/cultural/samurai-straw-hat-katana-traditional-japanese-clothes.jpg'
import cultural13 from './images/articles/cultural/untitled-43.jpg'
import cultural14 from './images/articles/cultural/riceTerraces.jpeg'
import fashion1 from './images/articles/fashion/0.jpeg'
import fashion2 from './images/articles/fashion/4-pen_5800.jpg'
import fashion3 from './images/articles/fashion/61c791d5415fd9402c57b3a18006f58f.jpg'
import fashion4 from './images/articles/fashion/97f946442c512d4c7103.jpeg'
import fashion5 from './images/articles/fashion/735d12c77b31dae1552519c0ec6f6060.jpg'
import fashion6 from './images/articles/fashion/109160.jpg'
import fashion7 from './images/articles/fashion/134387678.jpg'
import fashion8 from './images/articles/fashion/ade37327b452b2d4ce834affa88c04d2.jpg'
import fashion9 from './images/articles/fashion/d43a3e85b4184ec06d187063beec383c.jpg'
import fashion10 from './images/articles/fashion/dbb70147e6f843b9906d56dcc25b6288_th.jpeg'
import fashion11 from './images/articles/fashion/momo.jpeg'
import fashion12 from './images/articles/fashion/t1ddN0bO7nsc.jpg'
import fashion13 from './images/articles/fashion/TEM-11.jpg'
import fashion14 from './images/articles/fashion/tumblr_manzbb2Itg1rbbr68o1_1280.jpg'

const allReducers = combineReducers({
    banner: bannerReducer,
    featured: featuredReducer,
    articles: articlesReducer,
    toastr: toastrReducer
});

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const allStoreEnhanchers = devTools ? compose(
    applyMiddleware(thunk),
    devTools
) : compose(applyMiddleware(thunk));

const commonContent = '初期の村上隆や奈良美智を見いだしたギャラリスト小山登美夫が、アートギャラリーとの付き合い方をご案内';

export default createStore(
    allReducers,
    {
        banner: {
            life: [{
                uid: 0,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life1,
                ranking: 1
            }, {
                uid: 1,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life2,
                ranking: 2
            }, {
                uid: 2,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life3,
                ranking: 3
            }, {
                uid: 3,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life4,
                ranking: 4
            }, {
                uid: 4,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life5,
                ranking: 5
            }],
            culture: [{
                uid: 0,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural1,
                ranking: 1
            }, {
                uid: 1,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural2,
                ranking: 2
            }, {
                uid: 2,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural3,
                ranking: 3
            }, {
                uid: 3,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural4,
                ranking: 4
            }, {
                uid: 4,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural5,
                ranking: 5
            }],
            fashion: [{
                uid: 0,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion1,
                ranking: 1
            }, {
                uid: 1,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion2,
                ranking: 2
            }, {
                uid: 2,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion3,
                ranking: 3
            }, {
                uid: 3,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion4,
                ranking: 4
            }, {
                uid: 4,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion5,
                ranking: 5
            }],
            magazine: [{
                uid: 0,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life5,
                ranking: 1
            }, {
                uid: 1,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life4,
                ranking: 2
            }, {
                uid: 2,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life3,
                ranking: 3
            }, {
                uid: 3,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life2,
                ranking: 4
            }, {
                uid: 4,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life1,
                ranking: 5
            }]
        },
        featured: {
            life: [{
                uid: 5,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life6
            }, {
                uid: 6,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life7
            }, {
                uid: 7,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life8
            }],
            culture: [{
                uid: 5,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural6
            }, {
                uid: 6,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural7
            }, {
                uid: 7,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural8
            }],
            fashion: [{
                uid: 5,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion6
            }, {
                uid: 6,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion7
            }, {
                uid: 7,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion8
            }],
            magazine: [{
                uid: 5,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life8
            }, {
                uid: 6,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life7
            }, {
                uid: 7,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life6
            }]
        },
        articles: {
            life: [{
                uid: 8,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life9
            }, {
                uid: 9,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life10
            }, {
                uid: 10,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life11
            }, {
                uid: 11,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life12
            }, {
                uid: 12,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life13
            }, {
                uid: 13,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life14
            }, {
                uid: 14,
                date: '2018.12.10',
                category: 'Life',
                title: commonContent,
                image: life1
            }, {
                uid: 15,
                date: '2018.12.11',
                category: 'Life',
                title: commonContent,
                image: life2
            }],
            culture: [{
                uid: 8,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural9
            }, {
                uid: 9,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural10
            }, {
                uid: 10,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural11
            }, {
                uid: 11,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural12
            }, {
                uid: 12,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural13
            }, {
                uid: 13,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural14
            },{
                uid: 14,
                date: '2018.12.10',
                category: 'Culture',
                title: commonContent,
                image: cultural1
            }, {
                uid: 15,
                date: '2018.12.11',
                category: 'Culture',
                title: commonContent,
                image: cultural2
            }],
            fashion: [{
                uid: 8,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion9
            }, {
                uid: 9,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion10
            }, {
                uid: 10,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion11
            }, {
                uid: 11,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion12
            },{
                uid: 12,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion13
            }, {
                uid: 13,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion14
            }, {
                uid: 14,
                date: '2018.12.10',
                category: 'Fashion',
                title: commonContent,
                image: fashion1
            }, {
                uid: 15,
                date: '2018.12.11',
                category: 'Fashion',
                title: commonContent,
                image: fashion2
            }],
            magazine: [{
                uid: 8,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life11
            }, {
                uid: 9,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life12
            },{
                uid: 10,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life13
            }, {
                uid: 11,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life14
            }, {
                uid: 12,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life1
            }, {
                uid: 13,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life9
            }, {
                uid: 14,
                date: '2018.12.10',
                category: 'Magazine',
                title: commonContent,
                image: life10
            }, {
                uid: 15,
                date: '2018.12.11',
                category: 'Magazine',
                title: commonContent,
                image: life7
            }]
        },
    },
    allStoreEnhanchers
);