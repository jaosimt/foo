'use strict';

import { UPDATE_BANNER, SHOW_ERROR } from "../actions/bannerActions";

export default (state = '', { type, payload }) => {
    switch (type) {
        case UPDATE_BANNER:
        case SHOW_ERROR:
            return payload.banner;
        default:
            return state
    }
};
