'use strict';

import { UPDATE_ARTICLES, SHOW_ERROR } from "../actions/articlesActions";

export default (state = '', { type, payload }) => {
    switch (type) {
        case UPDATE_ARTICLES:
        case SHOW_ERROR:
            return payload.articles;
        default:
            return state
    }
};
