'use strict';

import { UPDATE_FEATURED, SHOW_ERROR } from "../actions/featuredActions";

export default (state = '', { type, payload }) => {
    switch (type) {
        case UPDATE_FEATURED:
        case SHOW_ERROR:
            return payload.featured;
        default:
            return state
    }
};
