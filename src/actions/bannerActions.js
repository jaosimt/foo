'use strict';

export const UPDATE_BANNER = 'banner:updateBanner';
export const SHOW_ERROR = 'banner:showError';

/**
 * @param banner
 * @returns {{type: string, payload: {banner: *}}}
 */
export function updateBanner(banner) {
    return {
        type: UPDATE_BANNER,
        payload: {
            banner: banner
        }
    }
}

/**
 * @param error
 * @returns {{type: string, payload: {banner: {error: *}}}}
 */
export function showError(error){
    return {
        type: SHOW_ERROR,
        payload: {
            banner: {
                error: error
            }
        }
    }
}

/**
 * @returns {function()}
 */
export function bannerReset(){
    return dispatch => {
        dispatch(updateBanner({}))
    }
}