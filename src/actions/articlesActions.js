'use strict';

export const UPDATE_ARTICLES = 'articles:updateArticles';
export const SHOW_ERROR = 'articles:showError';

/**
 * @param articles
 * @returns {{type: string, payload: {articles: *}}}
 */
export function updateArticles(articles) {
    return {
        type: UPDATE_ARTICLES,
        payload: {
            articles: articles
        }
    }
}

/**
 * @param error
 * @returns {{type: string, payload: {articles: {error: *}}}}
 */
export function showError(error){
    return {
        type: SHOW_ERROR,
        payload: {
            articles: {
                error: error
            }
        }
    }
}

/**
 * @returns {function()}
 */
export function articlesReset(){
    return dispatch => {
        dispatch(updateArticles({}))
    }
}