'use strict';

export const UPDATE_FEATURED = 'featured:updateFeatured';
export const SHOW_ERROR = 'featured:showError';

/**
 * @param featured
 * @returns {{type: string, payload: {featured: *}}}
 */
export function updateFeatured(featured) {
    return {
        type: UPDATE_FEATURED,
        payload: {
            featured: featured
        }
    }
}

/**
 * @param error
 * @returns {{type: string, payload: {featured: {error: *}}}}
 */
export function showError(error){
    return {
        type: SHOW_ERROR,
        payload: {
            featured: {
                error: error
            }
        }
    }
}

/**
 * @returns {function()}
 */
export function featuredReset(){
    return dispatch => {
        dispatch(updateFeatured({}))
    }
}