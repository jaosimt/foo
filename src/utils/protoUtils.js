'use strict';

Array.prototype.getWithID = Array.prototype.getWithID || function(uid) {
    return this.find(a => a.uid === uid || a.id === uid) || {}
};

Array.prototype.getWithKeyValuePair = Array.prototype.getWithKeyValuePair || function(keyValuePairObj) {
    if (isEmpty(keyValuePairObj)) {
        console.log('Empty getWithKeyValuePair arg: ', keyValuePairObj);
        return {}
    }
    
    let keys = 0, firstKey;
    for(let i in keyValuePairObj){
        if (keyValuePairObj.hasOwnProperty(i)) {
            if (keys === 0) { firstKey = i }
            keys++
        }
    }
    
    switch (keys) {
        case 0:
            console.log('Empty getWithKeyValuePair arg: ', keyValuePairObj);
            return {};
        case 1:
            break;
        default:
            console.log('Multiple getWithKeyValuePair arg: ', keyValuePairObj);
            console.log('  > Using first found key : ', firstKey);
    }
    
    return this.find(a => a[firstKey] === keyValuePairObj[firstKey]) || {}
};