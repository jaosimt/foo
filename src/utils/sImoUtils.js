'use strict';

/**
 * @type {{encode: (function(*=)), decode: (function(*=))}}
 */
const base64 = {
    encode: (string) => {
        return btoa(string);
    },
    decode: (base64) => {
        return atob(base64)
    }
};

/**
 * @param {file} file
 * @param {function} callback
 */
const readAsDataURL = (file, callback) => {
    if (file.constructor.name.toLowerCase() === "file" && typeof callback === 'function') {
        const fr = new FileReader();
        fr.onload = function(r) {
            callback(r.target.result)
        };
        
        fr.readAsDataURL(file);
    }
};

/**
 * @param {string} string
 * @returns {string} [lower cased]
 */
const toCamelCase = (string) => {
    if (!string) { return '' }
    
    return string.toLowerCase().replace(/ (.)/g, (m, p) => {
        return p.toUpperCase()
    })
};

/**
 * @param {string} string [capitalized]
 */
const capitalized = (string) => {
    return string.replace(/^\w| \w/igm, (a) => {
        return a.toUpperCase()
    })
};

const isVisible = (htmlElement) => {
    const rect = htmlElement.getBoundingClientRect();
    return rect.bottom > 0 &&
        rect.right > 0 &&
        rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */ &&
        rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */;
};

const isDefined = (obj) => {
    return (typeof obj).toLowerCase() !== "undefined"
};

const isString = (obj) => {
    return (typeof obj).toLowerCase() === "string"
};

const isNumber = (obj) => {
    return (typeof obj).toLowerCase() === "number"
};

const isBoolean = (obj) => {
    return (typeof obj).toLowerCase() === "boolean"
};

const isFunction = (obj) => {
    return (typeof obj).toLowerCase() === "function"
};

const isObject = (obj) => {
    return (typeof obj).toLowerCase() === "object" && (obj.constructor.name).toLowerCase() === "object"
};

const isArray = (obj) => {
    return (typeof obj).toLowerCase() === "object" && (obj.constructor.name).toLowerCase() === "array"
};

const isNumeric = (obj, strict)  => {
    if (isBoolean(strict) ? strict : false) {
        return isNumber(obj)
    } else {
        return isNumber(obj) || (isString(obj) && obj.match(/^[-+]*\d+(,\d\d\d)*(\.\d+)*$/));
    }
};

const isMobile = () => {
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(navigator.userAgent || navigator.vendor || window.opera) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw-(n|u)|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do(c|p)o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(-|_)|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-(m|p|t)|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c(-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac( |-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c(-|0|1)|47|mc|nd|ri)|sgh-|shar|sie(-|m)|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel(i|m)|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test((navigator.userAgent || navigator.vendor || window.opera).substr(0, 4)))
};

const isTablet = () => {
    return navigator.userAgent.match(/iPad|Tablet/i) !== null;
};

const isDecimal = (n) => {
    n = parseFloat(n);
    return isNumber(n) && n % 1 !== 0;
};

const isEmpty = (obj, evalRule) => {
    //----------------------------------------------------------------------------------------------------------------
    //  Argument evalRule       [Optional]
    //
    //      validValues         true | strict       >>  Strict mode or Boolean Only mode > ignores non Boolean objects
    //                          loose               >>  Loose mode > evaluates false on undefined objets only
    //                                                  e.g. will only check if object or node exists
    //                          false               >>  Default mode > evaluates true on non-empty objects
    //----------------------------------------------------------------------------------------------------------------
    
    evalRule = (evalRule === true) || (evalRule === "strict") ? "strict" : (evalRule !== false) && (evalRule !== "loose" ) ? false : evalRule;
    let obj_type = typeof(obj), is_empty = true;
    
    if (evalRule === "strict") {
        is_empty = !((obj_type === "boolean") && (obj === true));
    } else if (evalRule === "loose") {
        is_empty = obj_type === "undefined";
    } else {
        switch (obj_type) {
            case "boolean":
                is_empty = !obj;
                break;
            case "nodelist":
            case "array":
            case "object":
                is_empty = obj === null || false || Object.keys(obj).length < 1;
                break;
            case "number":
                is_empty = isNaN(obj);
                break;
            case "string":
                is_empty = obj.trim() === "";
                break;
            case "error":
            case "date":
            case "regexp":
            case "function":
                is_empty = false;
                break;
            default:
            // default _empty eq true as declared above when type is non of the above
        }
    }
    return is_empty;
};

const cloneArray = (array) => {
    if (!isArray(array)) {
        return [];
    }
    
    const newArray = [];
    for (let i = 0; i < array.length; i++) {
        newArray.push(array[i]);
    }
    
    return newArray
};

const after = (millisecondsTimeout, callback) => {
    if (isNumber(millisecondsTimeout) && isFunction(callback)) {
        setTimeout(callback, millisecondsTimeout);
    }
};

const loadSocialLoginSDK = () => {
    //------------------------------------------------------------------------------------------------------
    // FACEBOOK                                      https://developers.facebook.com/docs/facebook-login/web
    //------------------------------------------------------------------------------------------------------
    const FB_APP_ID = '191609164213909';   //pcprejan
    
    window.fbAsyncInit = function () {
        FB.init({
            appId: FB_APP_ID,
            cookie: true,  // enable cookies to allow the server to access the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.8
        });
        FB.AppEvents.logPageView();
    };
    
    //insert FB script to DOM
    (function (d, s, id) {
        let js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    
    //------------------------------------------------------------------------------------------------------
    // GOOGLE+
    //------------------------------------------------------------------------------------------------------
    // (function () {
    //   var po = document.createElement('script');
    //   po.type = 'text/javascript';
    //   po.async = true;
    //   po.src = '//apis.google.com/js/client:plusone.js';
    //   var s = document.getElementsByTagName('script')[0];
    //   s.parentNode.insertBefore(po, s);
    // })();
    
    //------------------------------------------------------------------------------------------------------
    // GOOGLE+ OAuth 2.0
    //------------------------------------------------------------------------------------------------------
    (function () {
        const po = document.createElement('script');
        po.type = 'text/javascript';
        po.async = true;
        po.src = '//apis.google.com/js/auth2.js';
        const s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(po, s);
    })();
    
};

const statusChangeCallback = (response, callback) => {
    let testAPI = () => {
        FB.api('/me', (response) => {
            consoleLog('FB.api/me: ', response);
            
            let statusEL = document.getElementById('status');
            //TODO what to do with the response object after being verified by fb.
            
            if (!response.error && response.id) {
                consoleLog('Logged in as: ', response.name);
                
                 //notify({
                 //  status: 'Success!',
                 //  message: `Logged in as ${response.name}.`
                 //}, 'success');
                
                if (statusEL) {
                    statusEL.innerHTML = 'This person has just logged in: ' + response.name;
                } else {
                    consoleLog('This person has just logged in: ' + response.name, response);
                }
                
                this.props.dispatch(facebookLogin(response.id))
            }
            
        })
    };
    
    let statusEL = document.getElementById('status');
    if (response.status === 'connected') {
        testAPI();
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        if (statusEL) {
            statusEL.innerHTML = 'Please log into this app.';
        } else {
            consoleLog('Please log into this app.')
        }
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        if (statusEL) {
            statusEL.innerHTML = 'Please log into Facebook.';
        } else {
            consoleLog('Please log into Facebook.');
        }
        
        FB.login(statusChangeCallback, {scope: 'email'});
    }
};

const fbClick = () => {
    console.color('fbClick', '', 'blue', '14px');
    
    FB.getLoginStatus((response) => {
        consoleLog("getLoginStatus");
        console.dir(response);
        statusChangeCallback(response);
    });
};

const gPlusClick = () => {
    if (window.navigator && window.navigator.standalone === true) {
        console.error('Login with Google is not supported to web apps added in Home Screen, Go to Safari or Chrome browser instead');
        //notify({
        //    status: 'Warning',
        //    message: 'Login with Google is not supported to web apps added in Home Screen, Go to Safari or Chrome browser instead.'
        //}, 'warning', {
        //    timeOut: 5000
        //});
        return;
    }
    
    console.color('gPlusClick', '', 'blue', '14px');
    let auth2;
    
    gapi.load('auth2', () => {
        auth2 = gapi.auth2.init({
            client_id: '335649926101.apps.googleusercontent.com',
            fetch_basic_profile: false,
            scope: 'profile'
        });
        
        // Sign the user in, and then retrieve their ID.
        auth2.signIn().then((googleUser) => {
            const google_id = auth2.currentUser.get().getId();
            console.log("google ID: ", google_id);
            this.props.dispatch(googleLogin(google_id));
            //TODO use id_token isntead of ID for backend auth
            //TODO https://developers.google.com/identity/sign-in/web/backend-auth
            const id_token = googleUser.getAuthResponse().id_token;
            console.log("google ID TOKEN: ", id_token);
        }).catch((e) => {
            console.log("Cancelled?", e);
        });
    });
};

/**
 * @param {boolean} allow
 */
const bodyOverflow = (allow) => {
    const body = document.querySelector('body');
    if (body) {
        if (allow) {
            body.classList.remove('no-overflow');
        } else {
            body.classList.add('no-overflow');
        }
    }
}

const getSelectedText = () => {
    let userSelection, ta;
    if (window.getSelection && document.activeElement) {
        if (document.activeElement.nodeName === "TEXTAREA" || (document.activeElement.nodeName === "INPUT" && (document.activeElement.getAttribute("type").toLowerCase() === "text" || document.activeElement.getAttribute("type").toLowerCase() === "tel"))) {
            ta = document.activeElement;
            userSelection = ta.value.substring(ta.selectionStart, ta.selectionEnd);
        } else {
            userSelection = window.getSelection();
        }
    } else {
        // all browsers, except IE before version 9
        if (document.getSelection) {
            userSelection = document.getSelection();
        }
        // IE below version 9
        else if (document.selection) {
            userSelection = document.selection.createRange();
        }
    }
    consoleLog("User Text Selection:" + userSelection);
    return String(userSelection);
};

let hasStorage = null;

const checkIfHasStorage = () => {
    let store = null;
    
    //if cookie is not enabled local storage is also not accessible
    if (!cookie.enabled()) {
        return false;
    }
    
    let fail,
        uid;
    
    try {
        uid = String(new Date);
        (store = window.localStorage).setItem(uid, uid);
        fail = store.getItem(uid) !== uid;
        store.removeItem(uid);
        fail && (store = false);
    } catch (exception) {
        return false;
    }
    
    if (fail) {
        consoleError("It seems that your browser is currently in private mode!");
    }
    
    return store && !fail;
};

const cookie = {
    enabled: function () {
        let cookieEnabled = (navigator.cookieEnabled);
    
        if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
            document.cookie = "testcookie";
            cookieEnabled = (document.cookie.indexOf("testcookie") !== -1);
        }
        return (cookieEnabled);
    },
    set: function (cName, cValue, expireDays) {
        if (cookie.enabled() && cName && cValue) {
            expireDays = isNumber(expireDays) ? expireDays : 7;
            const d = new Date();
            d.setTime(d.getTime() + (expireDays * 24 * 60 * 60 * 1000));
            const expires = "expires=" + d.toUTCString();
            document.cookie = cName + "=" + cValue + "; " + expires;
        }
    },
    get: function (cName) {
        if (!cookie.enabled()) {
            return "";
        }
        const name = cName + "=";
        const ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1);
            if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
        }
        return "";
    },
    remove: function (cookie) {
        if (cookie) {
            document.cookie = cookie + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
    }
};

const store = {
    set: (key, value, daysExpired) => {
        if (cookie.enabled()) {
            if (hasStorage === null) {
                hasStorage = checkIfHasStorage();
            }
    
            if (hasStorage) {
                window.localStorage.setItem(key, value)
            } else {
                cookie.set(key, value, daysExpired);
            }
        } else {
            consoleError("cookie is not enabled! respect, in one word!");
            return {};
        }
    },
    get: function (key) {
        if (hasStorage === null) {
            hasStorage = checkIfHasStorage();
        }
        
        const _getter = function (_key) {
            if (cookie.enabled()) {
                if (hasStorage) {
                    return window.localStorage.getItem(_key)
                } else {
                    return cookie.get(_key);
                }
            } else {
                consoleError("cookie is not enabled! respect, in one word!");
                return "";
            }
            
        };
        
        let item;
        if (key instanceof Array) {
            let i = key.length,
                found = false;
            
            while (i-- && !found) {
                item = _getter(key[i]);
                found = !isEmpty(item)
            }
        } else {
            item = _getter(key);
        }
        
        return item;
    },
    remove: function (_key) {
        if (hasStorage === null) {
            hasStorage = checkIfHasStorage();
        }
        
        if (hasStorage) {
            window.localStorage.removeItem(_key)
        } else {
            cookie.remove(_key)
        }
    }
};

const getObjVal = (obj, path, defVal, targetType) => {
    defVal = typeof defVal === "undefined" ? "" : defVal;
    targetType = typeof targetType === "string" ? targetType : "";
    
    if (typeof obj !== "object" || obj === null) {
        console.log("object param is not an object");
        return defVal;
    }
    
    let keys = path.split("."), value;
    for (let i = 0; i < keys.length; i++) {
        if (typeof obj[keys[i]] !== "undefined" && obj[keys[i]] !== null) {
            value = obj = obj[keys[i]];
        } else {
            return defVal;
        }
    }
    
    switch (targetType) {
        case "boolean":
            //noinspection Eslint
            value = eval(value) || defVal;
            break;
        case "numeric":
            let v = parseInt(value, 10);
            value = v || (isNumeric(v) ? v : defVal);
            break;
        default:
        // nothing here
    }
    
    return (value === null ? defVal : value);
};

const addJumpOnHover = (selector) => {
    const els = document.querySelectorAll(selector);
    els.forEach(e => {
        if (!e.classList.contains('cubic-bezier')) {
            e.classList.add('cubic-bezier');
        }
    
        e.onmouseenter = () => {
            e.classList.add('jump-hover')
        };
    
        e.onmouseout = () => {
            e.classList.remove('jump-hover')
        }
    })
}

const getNotificationError = (obj) => {
    consoleLog(obj, 'errorObj');
    
    const { request, response } = obj.error;
    
    consoleLog(request, 'request');
    consoleLog(response, 'response');
    
    let title = 'Error',
        message = 'Unidentified Error Occured!',
        list = [];
    
    if (response) {
        const { data: { errors: userErrors } } = response;
        
        title = response.status ? 'Error ' + response.status : title;
        message = response.statusText ? response.statusText : message;
        
        if (userErrors && userErrors.length > 0) {
            userErrors.forEach(e => {
                list.push(e.message)
            })
        }
    } else if (request && request.status === 0){
        title = 'Network Error';
        message = 'Server is unreachable!';
    } else {
        title = obj.error.status ? 'Error ' + obj.error.status : title;
        message = obj.error.statusText ? obj.error.statusText : message;
    }
    
    return {
        title: title,
        message: message,
        list: list
    }
};

module.exports = {
    cookie: cookie,
    store: store,
    base64: base64,
    getObjVal: getObjVal,
    readAsDataURL: readAsDataURL,
    toCamelCase: toCamelCase,
    capitalized: capitalized,
    isDefined: isDefined,
    isVisible: isVisible,
    isString: isString,
    isNumber: isNumber,
    isBoolean: isBoolean,
    isFunction: isFunction,
    isObject: isObject,
    isArray: isArray,
    isNumeric: isNumeric,
    isMobile: isMobile,
    isTablet: isTablet,
    isDecimal: isDecimal,
    isEmpty: isEmpty,
    cloneArray: cloneArray,
    after: after,
    bodyOverflow: bodyOverflow,
    loadSocialLoginSDK: loadSocialLoginSDK,
    addJumpOnHover: addJumpOnHover,
    getNotificationError: getNotificationError
};