'use strict';

import { isArray, isObject } from '../utils/sImoUtils'
import { toastr } from 'react-redux-toastr'
import PrettyToastr from  '../components/PrettyToastr'

const vectorizeText = require("vectorize-text");
const inArray = require('in-array');

/**
 * @param {string} mssg
 */
const limpyo = (mssg) => {
    return mssg.replace(/_/g, ' ').replace(/must be unique/, 'already exist!')
};

/**
 * @param {object} data {{string} title, {string} message, {array} list}
 * @param {string} type toastr types (info, warning, error, clear)
 */
const notify = function (data, type) {
    if (type === 'clear') {
        toastr.clean();
        return;
    }
    
    const toastrType = ['info', 'warning', 'error', 'clear'];
    
    const title = data.title || 'Error',
        message = data.message || 'Unspecified Error',
        list = data.list && isArray(data.list) ? data.list : [],
        timeOut = data.timeOut || 0;
    
    if (!inArray(toastrType, type)) {
        console.error('Invalid Toaster Type: ', type);
        console.error('             message: ', message);
        console.error('                list: ', list);
        return
    }
    
    toastr[type](title, {
        timeOut: timeOut,
        newestOnTop: true,
        preventDuplicates: true,
        position: "top-right",
        transitionIn: "bounceInDown",
        transitionOut: "bounceOutUp",
        progressBar: timeOut > 0,
        closeOnToastrClick: false,
        component: <PrettyToastr
            message={limpyo(message)}
            list={list.map(l => {
                return limpyo(l);
            })}
        />
    });
};

/**
 * @param {string} text
 * @param {object} options {triangles: {boolean: true}, width: {number: 100}, textBaseline: {string: 'top'}, stroke: {string: 'skyblue'}}
 * @returns {svg}
 */
const createSvg = (text, options) => {
    let captchaOptions = {
        triangles: true,
        width: 100,
        textBaseline: "top"
    };
    let stroke = options ? options.stroke || 'skyblue' : 'skyblue';
    
    if (isObject(options)) {
        for (let o in options) {
            if (options.hasOwnProperty(o) && captchaOptions.hasOwnProperty(o)) {
                captchaOptions[o] = options[o];
            }
        }
    }
    
    const complex = vectorizeText(text, captchaOptions);
    let svgHeader = '<?xml version="1.0" encoding="UTF-8" standalone="no"?> <svg id="svg2" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" width="' + captchaOptions.width + '">';
    let svgFooter = '</svg>';
    let svg = [];
    
    complex.cells.forEach(function(c) {
        for(let j=0; j<3; ++j) {
            const p0 = complex.positions[c[j]];
            const p1 = complex.positions[c[(j+1)%3]];
            svg.push('<line x1="' + p0[0] + '" y1="' + p0[1] + '" x2="' + p1[0] + '" y2="' + p1[1] + '" stroke-width="1" stroke="' + stroke + '"/>')
        }
    });
    
    return svgHeader + svg.join().replace(/^,|,$/g, '') + svgFooter;
};

module.exports = {
    notify: notify,
    createSvg: createSvg
};