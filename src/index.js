'use strict';

import React from 'react'
import ReactDOM from 'react-dom'
import './utils/protoUtils'
import App from './app'

import { Provider } from 'react-redux'

import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import ReduxToastr from 'react-redux-toastr'

import store from './store'

ReactDOM.render(
    <Provider store={store}>
        <div>
            <App/>
            <ReduxToastr
                timeOut={5000}
                newestOnTop={true}
                escapeHtml={true}
                preventDuplicates
                position="top-right"
                transitionIn="bounceInDown"
                transitionOut="bounceOutUp"
                progressBar
                closeOnToastrClick/>
        </div>
    </Provider>,
    document.getElementById('root')
);