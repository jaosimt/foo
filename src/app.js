'use strict';

import React, { Component } from 'react'

import { createSelector } from 'reselect'
import { toastr } from 'react-redux-toastr'
import {connect} from "react-redux"
import {debounce} from 'lodash'

import { isEmpty, bodyOverflow, after } from './utils/sImoUtils'
import { notify } from './utils/sImoReactUtils'

import "./fonts/mPlus1pRegular.css"
import './app.css';

import ImageSlider from "./components/ImageSlider"
import Article from "./components/Article"
import HideIf from "./components/HideIf";

class App extends Component {
    constructor(props) {
        super(props);
        
        this.collapsibleRef = React.createRef();
        this.selectedArticle = {};
        
        this.state = {
            menuItem: 'life',
            articleTrigger: false,
            windowHeight: '0',
        }
        
        this.menuItemClick.bind(this);
    
        this.initWindowResizeHandler = this.windowResizeHandler.bind(this);
        this.windowResizeHandler = debounce(this.windowResizeHandler.bind(this), 100);
    }
    
    menuItemClick = ({target}) => {
        if (target.classList.contains('selected')) {
            return;
        }
        
        this.setState({menuItem: target.className})
    }
    
    scrollToTop = () => window.scrollTo({
        top: 0,
        behavior: 'smooth',
    });
    
    articleCloseClick = () => {
        bodyOverflow(true);
    
        this.collapsibleRef.current.style.left = '101%';
    
        after(200, () => {
            this.setState({
                articleTrigger: false
            })
        })
    }
    
    articleClick = ({currentTarget}, size) => {
        let uid = -1;
        let data = this.props.articles;
        
        if (currentTarget) {
            uid = currentTarget.getAttribute('data-uid');
            
            if (currentTarget.classList.contains('featured')) {
                data = this.props.featured
            } else if (size === 'small') {
                data = this.props.banner
            }
    
            this.selectedArticle = data[this.state.menuItem].getWithID(parseInt(uid));
    
            if (!isEmpty(this.selectedArticle) && this.collapsibleRef.current) {
                this.setState({
                    articleTrigger: true
                })
        
                this.collapsibleRef.current.style.left = 0;
                
                bodyOverflow(false);
            } else {
                notify({
                    message: 'For some reason, selected article data was not found!  Please contact technical support'
                }, 'error')
            }
        } else {
            notify({
                message: 'For some reason, selected article data was not found!  Please contact technical support'
            }, 'error')
        }
    }
    
    searchClick = () => notify({
        title: 'Under Construction',
        message: '"Search" is still under construction.  Sorry for the inconvenience.',
        timeOut: 7000
    }, 'warning');
    
    moreClick = () => notify({
        title: 'Under Construction',
        message: '"More button functionality" is still under construction.  Sorry for the inconvenience.',
        timeOut: 7000
    }, 'warning');
    
    aboutUsClick = () => notify({
        title: 'Under Construction',
        message: '"About Us" is still under construction.  Sorry for the inconvenience.',
        timeOut: 7000
    }, 'warning');
    
    contactUsClick = () => notify({
        title: 'Under Construction',
        message: '"Contact Us" is still under construction.  Sorry for the inconvenience.',
        timeOut: 7000
    }, 'warning');
    
    dataPrivacyClick = () => window.open('https://www.privacy.gov.ph/data-privacy-act/', '_blank');
    operatorClick = () => notify({
        title: 'Under Construction',
        message: 'Under construction.  Sorry for the inconvenience.',
        timeOut: 7000
    }, 'warning');
    eightAdsClick = () => window.open('https://8card.net/', '_blank');
    instagramClick = () => window.open('https://www.instagram.com/jaosimt', '_blank');
    lineClick = () => window.open('https://line.me/', '_blank');
    facebookClick = () => window.open('https://facebook.com/jaosimt', '_blank');
    twitterClick = () => window.open('https://twitter.com/jaosimt', '_blank');
    
    windowResizeHandler = () => {
        this.setState({
            windowHeight: window.innerHeight
        });
    };
    
    componentDidMount() {
        this.initWindowResizeHandler();
        window.addEventListener('resize', this.windowResizeHandler);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.windowResizeHandler);
    }
    
    render() {
        const { banner, featured, articles } = this.props,
            { menuItem, articleTrigger, windowHeight } = this.state;
        
        return (
            <div className="foo">
                <div className={'foo-image-slider-wrapper'}>
                    <ImageSlider data={banner[menuItem]}/>
                </div>
                <div className={'foo-logo-section'}>
                    <div className={'logo'}/>
                    <div className={'btns'}>
                        <div className={'links-wrapper'}>
                            <div className={'links'}>
                                <span onClick={this.menuItemClick} className={menuItem === 'life' ? 'selected' : 'life'}>LIFE</span>
                                <span onClick={this.menuItemClick} className={menuItem === 'culture' ? 'selected' : 'culture'}>CULTURE</span>
                                <span onClick={this.menuItemClick} className={menuItem === 'fashion' ? 'selected' : 'fashion'}>FASHION</span>
                                <span onClick={this.menuItemClick} className={menuItem === 'magazine' ? 'selected' : 'magazine'}>MAGAZINE</span>
                            </div>
                        </div>
                        <div onClick={this.searchClick} className={'search'}/>
                    </div>
                </div>
                <div className={'foo-featured-section'}>
                    <div className={'label'}>PICKUP</div>
                    <div className={'featured'}>
                        {
                            featured[menuItem].map(f => {
                                return <Article
                                    className={'featured'}
                                    article={f}
                                    width={'31.15%'}
                                    onClick={this.articleClick}
                                />
                            })
                        }
                    </div>
                </div>
                <div className={'foo-articles-section'}>
                    <div className={'left-panel'}>
                        {
                            articles[menuItem].map(a => {
                                return <Article
                                    article={a}
                                    width={'47.3%'}
                                    onClick={this.articleClick}
                                />
                            })
                        }
                        <div className={'buttons'}>
                            <div onClick={this.moreClick} className={'more linear-transition horizontal-align'}>More</div>
                        </div>
                    </div>
                    <div className={'right-panel'}>
                        <div onClick={this.eightAdsClick} className={'ads'}/>
                        <div className={'ranking'}>
                            <div className={'label'}>- RANKING -</div>
                            {
                                banner[menuItem].map(r => {
                                    return <Article
                                        article={r}
                                        width={'100%'}
                                        size={'small'}
                                        ranked={true}
                                        onClick={this.articleClick}
                                    />
                                })
                            }
                        </div>
                        <div className={'buttons'}>
                            <div onClick={this.instagramClick} className={'instagram linear-transition'}/>
                            <div onClick={this.lineClick} className={'line linear-transition'}/>
                            <div onClick={this.facebookClick} className={'facebook linear-transition'}/>
                            <div onClick={this.twitterClick} className={'twitter linear-transition'}/>
                        </div>
                    </div>
                </div>
                <div className={'foo-footer-section'}>
                    <div className={'links middle-align'}>
                        <div onClick={this.scrollToTop} className={'link'}>Topページ</div>
                        <div onClick={this.aboutUsClick} className={'link'}>このサイトについて</div>
                        <div onClick={this.contactUsClick} className={'link'}>お問い合わせ</div>
                        <div onClick={this.operatorClick} className={'link'}>運営会社</div>
                        <div onClick={this.dataPrivacyClick} className={'link'}>個人情報保護について</div>
                    </div>
                    <div className={'clear-fix'}/>
                    <div className={'copyright'}>&copy; 2018 foo company</div>
                </div>
                <div style={{height: windowHeight}} key={'collapsibleRef'} ref={this.collapsibleRef} className={'foo-collapsible-content linear-transition'}>
                    <div className={'content'} style={{height: windowHeight}} >
                        <HideIf condition={!articleTrigger}>
                            <Article
                                size={'full'}
                                article={this.selectedArticle}
                                onClose={this.articleCloseClick}
                            />
                        </HideIf>
                    </div>
                </div>
            </div>
        );
    }
}

const bannerSelector = createSelector(
    state => state.banner,
    banner => banner
);

const featuredSelector = createSelector(
    state => state.featured,
    featured => featured
);

const articlesSelector = createSelector(
    state => state.articles,
    articles => articles
);

const mapStateToProps = createSelector(
    bannerSelector,
    featuredSelector,
    articlesSelector,
    (banner, featured, articles) => ({
        banner, featured, articles
    })
);

const actionsToProps = {
    dispatch: (dispatch) => {
        return dispatch
    }
}

export default connect(mapStateToProps, actionsToProps)(App)