"use strict";

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const src_path = path.resolve(__dirname, "src");
const build_path = path.resolve(__dirname, "dist");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const WebpackMonitor = require('webpack-monitor');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const exclusion = [
    path.resolve(__dirname, "NodeJS"),
    path.resolve(__dirname, "node_modules")
];

const env = process.env.NODE_ENV || 'development';
const devMode = env !== 'production';

let moduleRules = [
    {
        test: /\.js$/,
        exclude: exclusion,
        loader: 'babel-loader',
    },
    {
        test: /\.jsx?$/,
        exclude: exclusion,
        loaders: [
            "babel-loader?" + JSON.stringify({
                presets: ["react", "es2015", "stage-0"],
                plugins: ['react-html-attrs', "transform-react-inline-elements", "transform-class-properties", "transform-decorators-legacy"]
            })
        ]
    },
    {
        test: /\.svg$/,
        loader: "url-loader?limit=100000&name=styles/images/[hash:6]_[name].[ext]"
    },
    {
        test: /\.(gif|png|jp[e]*g|bmp)$/,
        loader: "url-loader?limit=100000&name=styles/images/[hash:6]_[name].[ext]"
    },
    {
        test: /\.(woff[2]*)$/,
        loader: "url-loader?limit=100000&name=styles/fonts/[hash:6]_[name].[ext]"
    },
    {
        test: /\.[ot]tf$/,
        loader: "url-loader?limit=100000&name=styles/fonts/[hash:6]_[name].[ext]"
    },
    {
        test: /\.eot$/,
        loader: "url-loader?limit=100000&name=styles/fonts/[hash:6]_[name].[ext]"
    },
    {
        test: /\.css$/,
        loaders: ["style-loader", "css-loader"]
    },
    {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
    },
    {
        test: /\.json$/,
        loader: "json-loader"
    }
];

let pluginConfig = [
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify(env)
        }
    })
];

if (!devMode) {
    moduleRules = moduleRules.concat([
        {
            test: /\.js[x]?$/,
            exclude: exclusion,
            loader: "webpack-strip?strip[]=debug,strip[]=console.log,strip[]=console.error,strip[]=console.group,strip[]=console.groupCollapsed,strip[]=console.groupEnd"
        }
    ]);
    
    pluginConfig = pluginConfig.concat([
        new CopyWebpackPlugin([
            { from: './src/manifest.json', to: build_path },
            { from: './src/images/favicon.ico', to: build_path }
        ]),
        new HtmlWebpackPlugin({
            hash: true,
            minify: {
                caseSensitive: true,
                html5                          : true,
                collapseWhitespace             : true,
                minifyCSS                      : true,
                minifyJS                       : true,
                minifyURLs                     : false,
                removeAttributeQuotes          : true,
                removeComments                 : true,
                removeEmptyAttributes          : true,
                removeOptionalTags             : true,
                removeRedundantAttributes      : true,
                removeScriptTypeAttributes     : true,
                removeStyleLinkTypeAttributese : true,
                useShortDoctype                : true
            },
            showErrors: false,
            filename: path.resolve(build_path, 'index.html'),
            template: path.resolve(src_path, 'index.tmpl.html'),
        }),
        new BundleAnalyzerPlugin({
            analyzerMode: 'server',
            analyzerPort: 8089,
            reportFilename: 'report.html',
            openAnalyzer: true,
            generateStatsFile: false,
            statsFilename: 'stats.json',
            statsOptions: null
        }),
        new WebpackMonitor({
            capture: true,
            target: '../webPackMonitor/stat.json',
            launch: true,
            port: 8090,
            excludeSourceMaps: true
        }),
    ]);
}

module.exports = {
    mode: env,
    entry: {
        "app": path.resolve(src_path, "index.js")
    },
    output: {
        path: build_path,
        filename: devMode ? "[name].min.js" : "[name]-[hash:6].min.js",
        chunkFilename: devMode ? "[name].min.js" : "[name]-[hash:6].min.js"
    },
    module: {
        rules: moduleRules
    },
    plugins: pluginConfig,
    optimization: {
        namedChunks: true,
        minimize: true,
        splitChunks: {
            chunks: devMode ? 'async' : 'all',
            minSize: 10000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name (module) {
                return String(module).toLocaleLowerCase().replace(/\[|\]/g, '');
            },
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        },
        minimizer: [
            new UglifyJsPlugin({
                parallel: true,
                cache: true,
                sourceMap: true,
                uglifyOptions: {
                    ecma: 6,
                    mangle: {
                        reserved: ['$super', '$', 'exports', 'require']
                    },
                    sourceMap: false,
                    compress: devMode ? true : {
                        drop_console: true
                    },
                    output: {
                        ecma: 6,
                        beautify: false,
                    }
                }
            })
        ]
    },
    resolve: {
        extensions: ['.js', '.json', '.jsx', '.css']
    },
    node: {
        fs: 'empty'
    }
};

// for Module not found: Error: Can't resolve 'fs' in '...' >> node: { fs: 'empty' }
// https://blog.appoptics.com/migrating-webpack-3-to-4/
// https://www.npmjs.com/package/uglifyjs-webpack-plugin
// https://jes.al/2018/04/optimizing-front-end-delivery-with-Webpack-4/